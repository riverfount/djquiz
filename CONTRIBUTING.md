# If you want to Contribute With de TODO-List API Project follow these steps:

We use [Poetry](https://python-poetry.org) to manage our virtualenv and dependencies. If you don't use the poetry don't forget to install the dev dependencies. You can know what are the dev dependencies into pyproject.toml file. This is self-explanatory. 

### There are some steps to take for contributing to the project

1. Clone the repository
2. Access the directory of your local repository
3. Install the project dependencies with Poetry

#### These steps in code:

```bash
git clone https://salsa.debian.org/riverfount/djquiz.git
cd djquiz
poetry install
```

### To run all tests

```bash
poety run pytest
```
