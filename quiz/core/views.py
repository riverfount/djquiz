from django.shortcuts import render

from quiz.core.models import Question


def home(request):
    return render(request, 'core/home.html')


def classification(request):
    return render(request, 'core/classification.html')


def questions(request, index: int):
    question = Question.objects.filter(is_available=True).order_by('id')[index - 1]
    context = {
        'question': question,
    }
    return render(request, 'core/game.html', context=context)
