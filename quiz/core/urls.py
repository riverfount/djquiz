from django.urls import path

from quiz.core import views

urlpatterns = [
    path('', views.home),
    path('classification', views.classification),
    path('questions/<int:index>', views.questions),
]
